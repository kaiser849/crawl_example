package com.example.cwral.scheduler.dao;

import com.example.cwral.scheduler.dto.BannerImgFileVO;
import com.example.cwral.scheduler.dto.BannerImgVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;

@Mapper
public interface NsBannerImgMapper {

    //파일존재여부 확인
    BannerImgFileVO checkExistImg(BannerImgFileVO param) throws Exception;
    //파일저장
    int insertBannerFile(BannerImgFileVO vo) throws Exception;
    int updateBannerFile(BannerImgFileVO vo) throws Exception;
    int checkNsBannerInfo(HashMap<String, Object> param) throws Exception;
    BannerImgFileVO checkExistGroupFile(HashMap<String, Object> param) throws Exception;
    int insertNsBannerInfo(BannerImgVO vo) throws Exception;
}
