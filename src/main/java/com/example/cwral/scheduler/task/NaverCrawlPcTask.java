package com.example.cwral.scheduler.task;


import com.example.cwral.scheduler.dto.BannerImgFileVO;
import com.example.cwral.scheduler.dto.BannerImgVO;
import com.example.cwral.scheduler.service.NsBannerImgService;
import com.example.cwral.utils.ImageUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class NaverCrawlPcTask {
    public static final String WEB_DRIVER_ID = "webdriver.chrome.driver";
    public static final String WEB_DRIVER_PATH = "/Users/shin/chromedriver/chromedriver";
    private static final Logger logger = LoggerFactory.getLogger(NaverCrawlPcTask.class);
    private static String preGroupId = "G_0";
    private static String rollGroupId = "";
    @Value("${server.file.imgPath}")
    String serverFilePath;
    @Autowired
    NsBannerImgService service;
    private String base_url = "https://www.naver.com";
    private WebDriver driver;

    @PostConstruct
    public void initRun() {
        runOnce();
    }

    //@Scheduled(cron = "*/30 0-5 * * * *")
    @Scheduled(fixedRate = 10000)// test
    public void userLocking() {
        logger.info("start crwaling naver PC");
        taskForNaverPC();
        logger.info("End crwaling naver PC");
    }

    //필요할지 몰라서..우선 지우지않음.
    public void runOnce() {
        System.setProperty(WEB_DRIVER_ID, WEB_DRIVER_PATH);
        driver = new ChromeDriver();
        base_url = "https://www.naver.com";
        driver.get(base_url);

    }

    public void taskForNaverPC() {
        try {

            Map<String, Object> resultParesMap = getParseHtml();

            String imgSrc = null;
            String imgName = null;
            String alt = null;

            if (!resultParesMap.get("type").toString().equals("R")) {

                imgSrc = resultParesMap.get("imgSrc").toString();
                imgName = resultParesMap.get("imgName").toString();
                alt = resultParesMap.get("alt") == null ? "" : resultParesMap.get("alt").toString();

                makeImages(imgSrc, imgName, alt, resultParesMap.get("type").toString());

            } else {

                List<WebElement> els = (List<WebElement>) resultParesMap.get("imgs");

                for (WebElement e : els) {
                    imgSrc = e.getAttribute("src");
                    imgName = imgSrc.substring(imgSrc.lastIndexOf("/") + 1);
                    alt = e.getAttribute("alt") == null ? null : e.getAttribute("alt").length() == 0 ? null : e.getAttribute("alt");
                    makeImages(imgSrc, imgName, alt, resultParesMap.get("type").toString());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Map<String, Object> getParseHtml() {

        HashMap<String, Object> resultMap = new HashMap<>();

        try {

            //      List<WebElement> a = driver.findElements(By.xpath("//div[@class=\"da_type_video over\" ]"));
            int videoSize = driver.findElements(By.xpath("//div[@class=\"da_type_video over\" ]")).size(); // 광고 iframe 여부 판단
            int rollingSize = driver.findElements(By.xpath("//div[@class=\"image_rolling_area time_board_type\"]")).size(); // 이미지 롤링이 있는지 가져오기

            System.out.println("video size :" + videoSize + ",  rollingSize : " + rollingSize);

            WebElement element1 = null;
            WebElement element2 = null;


            if (videoSize == 0 && rollingSize == 0) { //일반 화면
                driver.switchTo().frame("veta_top_inner_tgtLREC");
                //광고영역 엘리먼트 가져온다.
                element1 = driver.findElement(By.xpath("//div[@id=\"addiv\"]"));
                element2 = element1.findElement(By.tagName("img"));

                String imgSrc = element2.getAttribute("src");
                String alt = element2.getAttribute("alt");

                String imgName = imgSrc.substring(imgSrc.lastIndexOf("/") + 1);

                resultMap.put("type", "I");
                resultMap.put("imgSrc", imgSrc);
                resultMap.put("alt", alt);
                resultMap.put("imgName", imgName);


            } else if (videoSize == 0 && rollingSize > 0) {  //롤링화면
                element1 = driver.findElement(By.xpath("//div[@data-role=\"rollingWrapper\"]")); // 이미지 롤링 div 가져오기

                List<WebElement> list = element1.findElements(By.tagName("img"));
                List<WebElement> sortedList = new ArrayList<>();
                int index = 0;

                for (int i = 0; i < list.size(); i++) {
                    WebElement client = list.get(i);
                    //이미지 경로 가져오기
                    String alt = client.getAttribute("alt");
                    if (alt != null && alt.length() != 0) {
                        sortedList.add(client);
                        index = i;
                        break;
                    }
                }

                list.remove(index);
                sortedList.addAll(list);

                resultMap.put("type", "R");
                resultMap.put("imgs", sortedList);


            } else { //video 화면
                //동영상 광고영역 가져오기
                element1 = driver.findElement(By.xpath("//div[@class=\"da_type_video over\" ]"));
                element2 = element1.findElement(By.tagName("img"));

                //이미지 경로 가져오기
                String imgSrc = element2.getAttribute("src");
                String alt = element2.getAttribute("alt");

                String imgName = imgSrc.substring(imgSrc.lastIndexOf("/") + 1);

                resultMap.put("type", "M");
                resultMap.put("imgSrc", imgSrc);
                resultMap.put("alt", alt);
                resultMap.put("imgName", imgName);

            }


        } catch (Exception e) {

        }

        return resultMap;
    }

    @Transactional
    public void makeImages(String imgSrc, String imgName, String alt, String imgType) throws Exception {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        File bannerFile = null;
        File thumbFile = null;

        //배너파일을 가져와서 물리적으로 저장한다.
        bannerFile = ImageUtils.makeFileFromURL(imgSrc, serverFilePath, imgName);
        //배너파일이 저장되면 썸네일을 만든다.
        if (bannerFile != null) {
            thumbFile = ImageUtils.resizeImage(bannerFile, serverFilePath, 180, 50);

            if (thumbFile != null) {
                Date dt = new Date();
                int isResult = 0;

                //DB 저장
                BannerImgFileVO bannerInfo = new BannerImgFileVO();

                String dateTime = dateFormat.format(dt);

                bannerInfo.setImg_nm(imgName);
                bannerInfo.setImg_alt(alt);
                bannerInfo.setImg_path(bannerFile.getAbsolutePath());
                bannerInfo.setDvce_type("P");
                bannerInfo.setClct_dtm(dateTime);
                bannerInfo.setImg_thnl_nm(thumbFile.getName());
                bannerInfo.setImg_thnl_path(thumbFile.getAbsolutePath());
                bannerInfo.setBnnr_type(imgType);
                //업데이트를 한번해야한다.
                bannerInfo.setImg_group("G_");
                //배너 정보 테이블 저장 위한 VO
                BannerImgVO imgVO = new BannerImgVO();

                imgVO.setDvce_type("P");
                imgVO.setClct_dtm(dateTime);

                //배너 파일 정보가 있는지 체크
                BannerImgFileVO checkBannerVO = service.checkExistImg(bannerInfo);
                //저장된 파일이 없다면 저장하고..

                dateFormat = new SimpleDateFormat("yyyyMMddHH");

                if (checkBannerVO == null) {

                    isResult = service.insertBannerFile(bannerInfo);
                    imgVO.setImg_id(bannerInfo.getImg_id());

                    BannerImgFileVO groupFile = null;
                    if (isResult > 0) {
                        //롤링일경우는 alt 가 있는 애만 대표이미지
                        if (imgType.equals("R")) {
                            if (alt != null) {
                                rollGroupId = "G_" + bannerInfo.getImg_id();
                            } else {
                                //alt 가 없으면 기존꺼로 대체
                                rollGroupId = preGroupId;
                            }
                        } else {

                            //동일시간에 이미지가 없으니까..새로운 group id 를 만든다
                            rollGroupId = "G_" + bannerInfo.getImg_id();
                            /**
                             * 같은 시간내에는 동일회사 광고
                             * 그래서 같은시간에 이미 수집된 이미지가 있는지 체킹
                             */
                            HashMap<String, Object> param = new HashMap<>();
                            param.put("clct_dtm", dateFormat.format(new Date()));
                            param.put("group_id", "G_" + bannerInfo.getImg_id());

                            groupFile = service.checkExistGroupFile(param);

                            //동일회사 광고가 이미 있다면 groupId를 미리 저장된것과 맞춘다.
                            if (groupFile != null && !groupFile.getImg_group().equals("G_")) {
                                rollGroupId = groupFile.getImg_group();
                            } else {
                                groupFile = null;
                            }
                        }

                        //그룹 ID 저장
                        bannerInfo.setImg_group(rollGroupId);
                        //파일정보는 이미 들어가 있어서 group id 만 입력한다.
                        isResult = service.updateBannerFile(bannerInfo);

                        // 이전 그룹 Id 와 달라야 들어간다. 배너정보 테이블은 대표사진 정보만 들어가야함.!

                        if (imgType.equals("R")) {
                            if (!preGroupId.equals(rollGroupId)) {
                                service.insertNsBannerInfo(imgVO);
                            }
                        } else {
                            //롤링이 아닌경우는 동일 회사 광고가 없을 경우만 인서트
                            if (groupFile == null) {
                                service.insertNsBannerInfo(imgVO);
                            }
                        }

                    } else {
                        throw new Exception("insert Banner File Error");
                    }

                } else {//저장된 파일이 있다면 중복체크 후에 진행한다.

                    //이미지 id 넣기
                    imgVO.setImg_id(checkBannerVO.getImg_id());

                    HashMap<String, Object> param = new HashMap<>();
                    param.put("clct_dtm", dateFormat.format(new Date()));
                    param.put("img_nm", bannerInfo.getImg_nm());

                    isResult = service.checkNsBannerInfo(param);

                    //배너정보는 대표이미지만 들어가기때문에 그룹ID가 이전하고 달라야한다.
                    if (isResult == 0 && (!preGroupId.equals(rollGroupId))) {
                        service.insertNsBannerInfo(imgVO);
                    }
                }

                //이전 그룹Id 와 비교하기 위해서 이전 값을 옮겨놓는다.
                preGroupId = rollGroupId;
                // 다음을 위해서 초기화
                rollGroupId = "";
            } else {
                logger.error("error crawling PC , can't make thumbnail file");
            }
        } else {
            logger.error("error crawling PC , can't make file");
        }
    }

}
