package com.example.cwral.scheduler.task;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.cwral.parser.HtmlUnitParser;
import com.example.cwral.scheduler.dto.BannerImgFileVO;
import com.example.cwral.scheduler.service.NsBannerImgService;
import com.example.cwral.utils.ImageUtils;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.ScriptResult;
import com.gargoylesoftware.htmlunit.WebClient;

@Component
public class NaverCrawlMobileTask {
	private static Logger logger = LoggerFactory.getLogger(NaverCrawlMobileTask.class);

	@Autowired
	HtmlUnitParser parser;
	@Autowired
	NsBannerImgService service;

	@Value("${server.file.imgPath}")
	String serverFilePath;

	private String base_url = "https://m.naver.com";

	@PostConstruct
	public void initRun() {
		runOnce();
	}

//	@Scheduled(cron = "*/30 0-5 * * * *")
	@Scheduled(fixedRate=10000)// test
	public void userLocking() {
		logger.info("[Mobile Crawling] start crwaling naver Mobile");
//		taskForNaverMobile();
		logger.info("[Mobile Crawling] End crwaling naver Mobile");
	}

	//필요할지 몰라서..우선 지우지않음.
	public void runOnce() {
		logger.info("init crwaling naver Mobile");
	}
	  
	public void taskForNaverMobile() {

		WebClient webClient = new WebClient(BrowserVersion.CHROME);
		try {
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Document doc =  parser.htmlParser(base_url, webClient);

			File bannerFile = null;
			File thumbFile = null;

			Element element1 = null;
			Element element2 = null;

			element1 = doc.getElementById("HOME_AD");
			element2 = element1.getElementById("main_search_specialda_1");

			String imgSrc = null;
			String alt = null;
			String imgName = null;

			if(element2 == null)  {
				element2 = element1.getElementById("canvasAnimation");
				if(element2 != null) {
					ScriptResult result = parser.getPage().executeJavaScript("\n" +
							"  var canvas = document.getElementById('countdown_container').getElementsByTagName('canvas')[0];\n" +
							"   canvas.toDataURL();\n"
					);

					if (result != null) {
						
						String imgCode = result.getJavaScriptResult().toString();
						
						//배너파일을 가져와서 물리적으로 저장한다.
						bannerFile = ImageUtils.makeImgByBase64(imgCode, serverFilePath, imgName);
						
						//배너파일이 저장되면 썸네일을 만든다.
						if(bannerFile != null) {
							thumbFile = ImageUtils.resizeImage(bannerFile, serverFilePath, 180, 50);
						}
						
					}				
					
				}
			}else {

				element2 = element2.getElementsByTag("img").get(0);
				//이미지 경로 가져오기
				imgSrc = element2.attr("src");
				alt = element2.attr("alt");

				imgName = imgSrc.substring(imgSrc.lastIndexOf("/") + 1);

				//배너파일을 가져와서 물리적으로 저장한다.
				bannerFile = ImageUtils.makeFileFromURL(imgSrc, serverFilePath, imgName);
				//배너파일이 저장되면 썸네일을 만든다.
				if(bannerFile != null) {
					thumbFile = ImageUtils.resizeImage(bannerFile, serverFilePath, 180, 50);
				}

			}

			if(thumbFile != null) {

				int isResult = 0;
				String dateTime = dateFormat.format(new Date());

				//DB 저장
				BannerImgFileVO bannerInfo = new BannerImgFileVO();
				bannerInfo.setImg_nm(imgName);
				bannerInfo.setImg_alt(alt);
				bannerInfo.setImg_path(bannerFile.getAbsolutePath());
				bannerInfo.setDvce_type("M");
				bannerInfo.setBnnr_type("I");
				bannerInfo.setClct_dtm(dateTime);
				bannerInfo.setImg_thnl_nm(thumbFile.getName());
				bannerInfo.setImg_thnl_path(thumbFile.getAbsolutePath());
				bannerInfo.setImg_group("G_");

				isResult = service.processInsertMobileBanner(bannerInfo, dateTime , "M");

				if(isResult > 0)  {
					logger.debug("[Mobile Crawling] Success Insert Banner");
				}

			}else{
				logger.error("[Mobile Crawling] error crawling Mobile , can't make file");
			}
		}catch(Exception e) {
			logger.error("[Mobile Crawling] error crawling Mobile {}", e.getMessage());
		}finally {
			parser.close(webClient);
		}
	}
}
