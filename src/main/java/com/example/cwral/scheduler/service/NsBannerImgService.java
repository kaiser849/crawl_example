package com.example.cwral.scheduler.service;

import com.example.cwral.scheduler.dao.NsBannerImgMapper;
import com.example.cwral.scheduler.dto.BannerImgFileVO;
import com.example.cwral.scheduler.dto.BannerImgVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

@Service
public class NsBannerImgService {

    @Autowired
    NsBannerImgMapper mapper;


    public BannerImgFileVO checkExistImg(BannerImgFileVO param) throws Exception {
        return mapper.checkExistImg(param);
    }

    public int insertBannerFile(BannerImgFileVO vo) throws Exception {
        return mapper.insertBannerFile(vo);
    }

    public  int updateBannerFile(BannerImgFileVO vo) throws Exception {
        return mapper.updateBannerFile(vo);
    }

    public int checkNsBannerInfo(HashMap<String, Object> param) throws Exception {
        return mapper.checkNsBannerInfo(param);
    }

    public BannerImgFileVO checkExistGroupFile(HashMap<String, Object> param) throws Exception{
        return mapper.checkExistGroupFile(param);
    }

    public int insertNsBannerInfo(BannerImgVO vo) throws Exception {
        return mapper.insertNsBannerInfo(vo);
    }

    @Transactional
    public int processInsertMobileBanner(BannerImgFileVO bannerInfo,String dateTime,  String dvceType) throws Exception {
        int isResult = 0;

        BannerImgFileVO checkBannerVO = mapper.checkExistImg(bannerInfo);
        //배너 정보 테이블 저장 위한 VO
        BannerImgVO imgVO = new BannerImgVO();
        imgVO.setDvce_type(dvceType);
        imgVO.setClct_dtm(dateTime);

        if(checkBannerVO == null) {
            isResult = mapper.insertBannerFile(bannerInfo);
            imgVO.setImg_id(bannerInfo.getImg_id());

            String rollGroupId = "G_" + bannerInfo.getImg_id();
            //그룹 ID 저장
            bannerInfo.setImg_group(rollGroupId);
            //파일정보는 이미 들어가 있어서 group id 만 입력한다.
            isResult = mapper.updateBannerFile(bannerInfo);

            if(isResult > 0) {
                mapper.insertNsBannerInfo(imgVO);
            }else{
                throw new Exception("Insert Error");
            }
        } else {//저장된 파일이 있다면 중복체크 후에 진행한다.

            //이미지 id 넣기
            imgVO.setImg_id(checkBannerVO.getImg_id());

            HashMap<String, Object> param = new HashMap<>();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
            param.put("clct_dtm", dateFormat.format(new Date()));
            param.put("img_nm", bannerInfo.getImg_nm());

            isResult = mapper.checkNsBannerInfo(param);

            if(isResult == 0) {
                isResult = mapper.insertNsBannerInfo(imgVO);
            }
        }

        return isResult;
    }
}
