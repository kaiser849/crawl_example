package com.example.cwral.scheduler.dto;


import lombok.Data;

@Data
public class BannerImgFileVO {

    private Integer img_id;          // 파일 id
    private String bnnr_type;        //이미지 타입 I(Image), R(rolling), M(Mix)
    private String img_group;     // 이미지 그룹 ID
    private String dvce_type;       //디바이스  타입 P(PC), M(mobile)
    private String img_nm;           //파일명;
    private String img_alt;          //이미지 내용*/,
    private String img_path;         //파일 물리경로(원본이미지 저장)
    private String img_thnl_nm;     //썸네일명;
    private String img_thnl_path;  //썸네일 물리경로(resize)
    private String clct_dtm;        //등록 일자

}
