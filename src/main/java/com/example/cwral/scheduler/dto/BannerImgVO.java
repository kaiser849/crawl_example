package com.example.cwral.scheduler.dto;


import lombok.Data;

@Data
public class BannerImgVO {

    private Integer bnnr_id;         //배너 id',
    private Integer img_id;         //배너 파일 (banner_img_file.img_id)',
    private String  dvce_type;      //배너 타입 P(PC), M(mobile)',
    private Integer inds_code;     //업종 코드 (inds_cate.inds_code)',
    private String cmpn_nm;        //업체명',
    private String memo;           //메모',
    private String edit_yn;        //수정 여부',
    private String clct_dtm;         //등록 일자',
    private String reg_wdy;       //등록 요일 0(월) ~ 6(일)',

}
