package com.example.cwral.parser;

import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;


@Component
public class HtmlUnitParser {
    Logger logger = LoggerFactory.getLogger(HtmlUnitParser.class);

    private HtmlPage page;
    
    public Document htmlParser(String basUrl, WebClient webClient)  {

    	System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "off");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "off");
        //불필요 로그 지우기

        //크롬버전으로
        Document doc = null;
        try {
            //url 설정
            URL url = new URL(basUrl);
            //css 인정
            webClient.getOptions().setCssEnabled(true);
            //스크립트 오류 무시
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setJavaScriptEnabled(true);
            //대기시간

            this.page = webClient.getPage(url);

                webClient.waitForBackgroundJavaScript(30000);
                webClient.waitForBackgroundJavaScriptStartingBefore(30000);

            page.initialize();

            doc = Jsoup.parse(page.asXml());

        }catch(IOException | ScriptException e) {
        	logger.debug("[HtmlUnitParser Error , {}", e.getMessage());
        }catch(Exception e) {
        	logger.debug("[HtmlUnitParser Error , {}", e.getMessage());
        }

        return doc;
    }


    public HtmlPage getPage () {
        return this.page;
    }
    
   public void close (WebClient webClient) {
	   try {
		   
		   if( webClient !=null) {
			   webClient.getCurrentWindow().getJobManager().removeAllJobs();
			   webClient.close();
			   System.gc();
		   }
	   }catch(Exception e) {
		   logger.debug("[HtmlUnitParser Error , {}", e.getMessage());
	   }
   }

}
